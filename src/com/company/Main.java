package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] aDigits = {5,2,4,6,1,3,2,6,1,2,4,5,63,68,98,45,4,36,7,678,3,4,2,7,976,54,457,5,43,453,4,547,56,8};
        System.out.println("Default array: " + Arrays.toString(aDigits) + "\n");
        Sort(aDigits, 1, aDigits.length);
        System.out.println("Sorted array:" + Arrays.toString(aDigits));
    }

    private static void Sort(int[] aSorting, int StartItemNumber, int LastItemNumber){
        if(StartItemNumber < LastItemNumber){
            // Index of array middle.
            int iMiddle = (int)Math.floor((StartItemNumber + LastItemNumber) / 2);
            Sort(aSorting, StartItemNumber, iMiddle);
            Sort(aSorting, iMiddle+1, LastItemNumber);
            Merge(aSorting, StartItemNumber, iMiddle, LastItemNumber);
        }
    }

    private static void Merge(int[] aSorting, int iStart, int iMiddle, int iEnd) {
        int iSortingStart = iStart - 1;
        int iSortingMiddle = iMiddle;
        int bufferLength = iEnd - iSortingStart;
        int[] buffer = new int[bufferLength];
        int iBuffer = 0;
        do {
            if (aSorting[iSortingStart] < aSorting[iSortingMiddle])
            {
                buffer[iBuffer++] = aSorting[iSortingStart];
                if(iSortingStart + 1 < iMiddle)
                    iSortingStart++;
                else {
                    iSortingStart = iSortingMiddle;
                    break;
                }
            }
            else {
                buffer[iBuffer++] = aSorting[iSortingMiddle];
                if (iSortingMiddle + 1 < iEnd)
                    iSortingMiddle++;
                else
                    break;
            }
        } while (iBuffer < bufferLength);

        while(iBuffer < bufferLength)
        {
            buffer[iBuffer++] = aSorting[iSortingStart++];
        }

        iStart--;
        for (int sortedItem: buffer) {
            aSorting[iStart++] = sortedItem;
        }
    }
}